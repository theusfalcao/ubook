import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  createPersistedState({
    key: 'ubook',
    paths: ['contacts']
  })(store)
}
