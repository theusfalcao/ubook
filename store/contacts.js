import { nanoid } from 'nanoid'

export const state = () => ([])

export const mutations = {
  add (state, contact) {
    state.push(contact)
  },

  edit (state, { uid, name, email, phone }) {
    const contact = mutations.findByUID(state, uid)
    if (!contact) { return }
    if (name) { contact.name = name }
    if (email) { contact.email = email }
    if (phone) { contact.phone = phone }
  },

  findByUID (state, uid) {
    const contact = state.filter(item => item.uid === uid)[0]
    return contact
  },

  removeByUID (state, uid) {
    const contact = mutations.findByUID(state, uid)

    if (contact) {
      const index = state.indexOf(contact)
      state.splice(index, 1)
    }
  }
}

export const actions = {
  new ({ commit }, { name, email, phone }) {
    const contact = { name, email, phone, uid: nanoid() }
    commit('add', contact)
    return contact
  },

  editByUID ({ commit }, { uid, contact }) {
    commit('edit', { uid, ...contact })
  }
}

export const getters = {
  ordened (state) {
    const contacts = [...state]
    contacts.sort((a, b) => {
      const nameA = a.name.toUpperCase()
      const nameB = b.name.toUpperCase()

      if (nameA > nameB) {
        return 1
      }

      if (nameA < nameB) {
        return -1
      }

      return 0
    })
    return contacts
  }
}
